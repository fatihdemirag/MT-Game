package fatihdemirag.net.mtgame;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class Sonuc extends AppCompatActivity {

    TextView puanSayi,dogruSayi,enFazlaPuan;
    TextView baslik,puanBaslik,dogruBaslik;
    String gelenPuan,gelenDogru;
    Intent intent;
    Button yenidenOyna;
    SharedPreferences kayit;
    SharedPreferences.Editor editor;
    SharedPreferences oku;
    InterstitialAd gecisReklam;
    AdRequest adRequest;

    public void tostMesaj(String mesaj){
        Toast.makeText(this,mesaj,Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onBackPressed() {
        finish();
        intent=new Intent(Sonuc.this,MainActivity.class);
        startActivity(intent);
        super.onBackPressed();
    }
    public void EnBuyukPuanKaydet()
    {
        kayit=getSharedPreferences("buyukPuan", Context.MODE_PRIVATE);
        editor=kayit.edit();

        oku=getSharedPreferences("buyukPuan",Context.MODE_PRIVATE);
        int puanOku=oku.getInt("puan",0);

        //editor.putInt("puan",-1);
        int puanS=Integer.parseInt(puanSayi.getText().toString());
        if (puanS>puanOku)
        {
            int bPuan=Integer.parseInt(puanSayi.getText().toString());
            editor.putInt("puan",bPuan);
        }
        editor.apply();
        editor.commit();
        enFazlaPuan.setText(String.valueOf(puanOku));

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sonuc);
        getSupportActionBar().hide();

        adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice("E37F7D3F87D714993F6FD684253F6A43").build();
        ReklamGoster();

        puanSayi=(TextView)findViewById(R.id.puanSayisi);
        dogruSayi=(TextView)findViewById(R.id.dogruSayi);
        baslik=(TextView)findViewById(R.id.baslik);
        puanBaslik=(TextView)findViewById(R.id.puanBaslik);
        dogruBaslik=(TextView)findViewById(R.id.dogruBaslik);
        yenidenOyna=(Button)findViewById(R.id.yenidenOyna);
        yenidenOyna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (reklamKontrol)
                    gecisReklam.show();
                else
                {
                    intent=new Intent(Sonuc.this,Baslangic.class);
                    startActivity(intent);
                }


            }
        });
        enFazlaPuan=(TextView)findViewById(R.id.enFazlaPuan);

        gelenPuan = getIntent().getExtras().getString("puan");
        puanSayi.setText(gelenPuan);

        gelenDogru=getIntent().getExtras().getString("dogru");
        dogruSayi.setText(gelenDogru);
        EnBuyukPuanKaydet();
    }
    boolean reklamKontrol;
    public void ReklamGoster()
    {
                gecisReklam = new InterstitialAd(getApplicationContext());

                gecisReklam.setAdUnitId("ca-app-pub-9361831426651608/6717979844");

                gecisReklam.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        if (gecisReklam.isLoaded()) {
//                            gecisReklam.show();
                            reklamKontrol=true;
                        }
                        else
                            reklamKontrol=false;
                        super.onAdLoaded();
                    }

                    @Override
                    public void onAdClosed() {
                        adRequest = new AdRequest.Builder().build();
                        intent = new Intent(Sonuc.this,Baslangic.class);
                        startActivity(intent);
                        super.onAdClosed();
                    }
                });
                gecisReklam.loadAd(adRequest);
    }
}
