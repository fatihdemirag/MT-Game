package fatihdemirag.net.mtgame;//Developer FXD-FATİH DEMİRAĞ

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.KeyListener;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Random;

public class Baslangic extends AppCompatActivity {

    Button cozButton,soruGec;
    TextView s2,s1,isaret,puan,textView2,altin;
    EditText girdi;
    long sayi1;
    long sayi2;
    byte sayac=0;
    Random rand=new Random();
    String[] isaretler={"+","-","*","/","/"};
    long toplamSonuc,cikarmaSonuc,carpmaSonuc,bolmeSonuc;
    Integer puanToplam=new Integer(0);
    Intent gidenPuanlar,intent;
    SharedPreferences kayit,oku;
    SharedPreferences.Editor editor;
    int altinGet=0;
    AdView adView;
    AdRequest adRequest;

    public static CountDownTimer countDownTimer;

    public void tostMesaj(String mesaj){
        Toast.makeText(this,mesaj,Toast.LENGTH_LONG).show();
    }
    @Override
    public void onBackPressed(){
        countDownTimer.cancel();
        finish();
        intent=new Intent(Baslangic.this, MainActivity.class);
        startActivity(intent);
        super.onBackPressed();
    }
    public void rand()
    {
        sayi1=rand.nextInt(100-1)+1;
        sayi2=rand.nextInt(10-1)+1;
        s1.setText(String.valueOf(sayi1));
        s2.setText(String.valueOf(sayi2));
        String isaretRand=isaretler[rand.nextInt(3)];
        isaret.setText(isaretRand);
        switch (isaretler[rand.nextInt(3)])
        {
            case "+":
                isaret.setText("+");
                toplamSonuc=sayi1+sayi2;
                break;
            case "-":
                isaret.setText("-");
                cikarmaSonuc=sayi1-sayi2;
                if (cikarmaSonuc==0)
                    rand();
                break;
            case "*":
                isaret.setText("*");
                carpmaSonuc=sayi1*sayi2;
                break;
            case "/":
                if(sayi1%sayi2==0)
                {
                    isaret.setText("/");
                    bolmeSonuc=sayi1/sayi2;
                }
                else
                {
                    sayi1=rand.nextInt(100-1)+1;
                    sayi2=rand.nextInt(10-1)+1;
                    s1.setText(String.valueOf(sayi1));
                    s2.setText(String.valueOf(sayi2));
                }
                break;
            default:
                tostMesaj("İşlem sırasında hata oluştu");
        }
    }
    public void islemler()
    {
        try {
            if (girdi.getText().toString().equals(String.valueOf(toplamSonuc))) {
                puanToplam += 1;
                puan.setText(puanToplam + "");
                girdi.setText("");
                sayac++;
                countDownTimer.start();
                AltinOku();
            } else if (girdi.getText().toString().equals(String.valueOf(cikarmaSonuc))) {
                puanToplam += 1;
                puan.setText(puanToplam + "");
                girdi.setText("");
                sayac++;
                countDownTimer.start();
                AltinOku();
            } else if (girdi.getText().toString().equals(String.valueOf(carpmaSonuc))) {
                puanToplam += 2;
                puan.setText(puanToplam + "");
                girdi.setText("");
                sayac++;
                countDownTimer.start();
                AltinOku();
            } else if (girdi.getText().toString().equals(String.valueOf(bolmeSonuc))) {
                puanToplam += 2;
                puan.setText(puanToplam + "");
                girdi.setText("");
                sayac++;
                countDownTimer.start();
                AltinOku();
            } else {
                countDownTimer.cancel();
                s1.setText("");
                s2.setText("");
                gidenPuanlar = new Intent(Baslangic.this, Sonuc.class);
                gidenPuanlar.putExtra("puan", puanToplam.toString());
                String sayacString = String.valueOf(sayac);
                gidenPuanlar.putExtra("dogru", sayacString.toString());
                startActivity(gidenPuanlar);
            }
        } catch (NumberFormatException n)
        {
            tostMesaj("Lütfen Düzgün Sayı Giriniz");

        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    int sure=16000;
    public void Sure()
    {
        try {
            countDownTimer = new CountDownTimer(sure,1000) {
                public void onTick (long l)
                {
                    textView2.setVisibility(View.VISIBLE);
                    textView2.setText(String.valueOf(l/1000));

                }
                @Override
                public void onFinish () {
                    textView2.setVisibility(View.VISIBLE);
                    s1.setText("");
                    s2.setText("");
                    gidenPuanlar = new Intent(Baslangic.this, Sonuc.class);
                    gidenPuanlar.putExtra("puan", puanToplam.toString());
                    String sayacString = String.valueOf(sayac);
                    gidenPuanlar.putExtra("dogru", sayacString.toString());
                    startActivity(gidenPuanlar);
                }
            }.start();
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    public void AltinKaydet()
    {
        oku=getSharedPreferences("altin adedi",Context.MODE_PRIVATE);
        altinGet=oku.getInt("altin",0);

        kayit=getSharedPreferences("altin adedi", Context.MODE_PRIVATE);
        editor=kayit.edit();

        if (sayac%3==0 && sayac!=0) {
            altinGet += 1;
            editor.putInt("altin", altinGet);
        }
        editor.apply();
        editor.commit();
    }
    public void AltinOku()
    {
        oku=getSharedPreferences("altin adedi",Context.MODE_PRIVATE);
        int altinGet=oku.getInt("altin",0);
        altin.setText(altinGet+"");
    }
    public void SoruGec()
    {
        oku=getSharedPreferences("altin adedi",Context.MODE_PRIVATE);
        altinGet=oku.getInt("altin",0);
        if (altinGet==5 || altinGet>5) {
            soruGec.setVisibility(View.VISIBLE);
            AltinOku();
            soruGec.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    rand();
                    kayit=getSharedPreferences("altin adedi", Context.MODE_PRIVATE);
                    editor=kayit.edit();
                    if (altinGet!=0 || altinGet>0 || altinGet<=5)
                    {
                        editor.putInt("altin", altinGet - 5);
                        soruGec.setVisibility(View.INVISIBLE);
                    }
                    if (altinGet<5)
                        soruGec.setVisibility(View.INVISIBLE);
                    editor.apply();
                    editor.commit();
                    AltinOku();
                }
            });
        }
    }
    public void onPause()
    {
        countDownTimer.cancel();
        sure=Integer.valueOf(textView2.getText().toString())*1000;
        super.onPause();
    }
    public void onRestart()
    {
        Sure();
        girdi.setText("");
        if (sure/1000==0)
        {
            Sure();
            rand();
        }
        super.onRestart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_baslangic);
        getSupportActionBar().hide();

        adView = (AdView) this.findViewById(R.id.adView);
        adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice("E37F7D3F87D714993F6FD684253F6A43").build();
        adView.loadAd(adRequest);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        cozButton=(Button)findViewById(R.id.cozButton);
        s1=(TextView) findViewById(R.id.sayi1);
        s2=(TextView)findViewById(R.id.sayi2);
        isaret=(TextView)findViewById(R.id.isaret);
        puan=(TextView)findViewById(R.id.puan);
        girdi=(EditText)findViewById(R.id.girdi);
        textView2=(TextView)findViewById(R.id.textView2);
        altin=(TextView)findViewById(R.id.altin);
        soruGec=(Button)findViewById(R.id.soruGec);

        rand();
        Sure();
        AltinOku();
        SoruGec();
        cozButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                        if(girdi.getText().toString().equals("-"))
                        {
                            tostMesaj("Lütfen Bir Sayı Giriniz");
                        }
                        else if (girdi.getText().toString().equals("-0"))
                        {
                            tostMesaj("Sıfır Negatif veya Pozitif Değildir");
                        }
                        else if (girdi.getText().toString().trim().equals("."))
                        {
                            tostMesaj("Lütfen Bir Sayı Giriniz");
                        }
                        else if (girdi.getText().toString().toCharArray()[0]=='.')
                        {
                            tostMesaj("Sayı Nokta İle Başlayamaz");
                        }
                        else if(girdi.getText().toString().equals("0") || girdi.getText().toString().equals("00") || girdi.getText().toString().equals("000")
                                || girdi.getText().toString().equals("0000"))
                            tostMesaj("Sıfırdan Farklı Bir Sayı Giriniz");
                        else {
                            islemler();
                            rand();
                            SoruGec();
                            AltinKaydet();
                        }
                }
                catch (Exception e)
                {
                    tostMesaj("İşlem Sırasında Hata Oluştu");
                }
            }
        });
        girdi.setOnKeyListener(new View.OnKeyListener(){
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (keyEvent.getAction()==KeyEvent.ACTION_DOWN)
                {
                    switch (i)
                    {
                        case KeyEvent.KEYCODE_ENTER:
                        {
                            girdi.setRawInputType(Configuration.KEYBOARD_12KEY);
                            islemler();
                            rand();
                            SoruGec();
                            AltinKaydet();
                        }
                    }
                }
                return false;
            }
        });

    }
}